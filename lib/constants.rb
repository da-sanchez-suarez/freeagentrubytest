class Constants

    DATE_NOT_FOUND_ERROR = "Date requested not in our dataset."
    CURRENCY_NOT_FOUND_ERROR = "Currency requested not available in our dataset at least for that date:"
    CURRENCY_DATA_FILE = "data/eurofxref-hist-90d.json"
    BASE_CURRENCY = "EUR"
end