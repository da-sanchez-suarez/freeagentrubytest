require 'json'

class JsonParser

    def self.parseJsonFileIntoObject(fileName)
        file = open(fileName)
        jsonText = file.read
        return JSON.parse(jsonText)
    end
end