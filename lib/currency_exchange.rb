require 'dao/currency_dao'
require 'constants'
require 'date'
module CurrencyExchange

  # Return the exchange rate between from_currency and to_currency on date as a float.
  # Raises an exception if unable to calculate requested rate.
  # Raises an exception if there is no rate for the date provided.
  def self.rate(date, from_currency, to_currency)
    dateFound = false
    currencyFromFound = false
    currencyToFound = false
    result = -1.0
    begin
      strDate = date.to_s ## date.to_formatted_s(:iso8601)
      dateData = CurrencyDAO::fetchCurrencyExchangeByDate(strDate)
      
      if strDate == nil || dateData != nil
        dateFound = true
      else 
        raise
      end 
      exchangeFrom = from_currency == Constants::BASE_CURRENCY ? 1 : dateData[from_currency]
      if exchangeFrom != nil
        currencyFromFound = true
      else
        raise
      end
      exchangeTo = to_currency == Constants::BASE_CURRENCY ? 1 : dateData[to_currency]
      if exchangeTo != nil
        currencyToFound = true
      else
        raise
      end
      result = exchangeTo / exchangeFrom
    rescue => exception
      if !dateFound
        raise Constants::DATE_NOT_FOUND_ERROR
      elsif !currencyFromFound
        raise Constants::CURRENCY_NOT_FOUND_ERROR + from_currency
      elsif !currencyToFound
        raise Constants::CURRENCY_NOT_FOUND_ERROR + to_currency
      else
        raise exception
      end
    end
    return result
  end

end
