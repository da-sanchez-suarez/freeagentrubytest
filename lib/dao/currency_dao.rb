require 'constants'
require 'json_parser'
class CurrencyDAO

    @@currencyData = JsonParser.parseJsonFileIntoObject(Constants::CURRENCY_DATA_FILE)

    ## We wouldn't usually put our datasource on a file
    ## this might be redundant for this case but not for
    ## real life scenarios where we would be using either
    ## a database or an API.
    def self.fetchCurrencyExchangeByDate(date)
        puts date
        return @@currencyData[date]
    end
end